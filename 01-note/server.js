const express = require('express')
const expressGraphQl = require('express-graphql')
const { GraphQLSchema, GraphQLString, GraphQLObjectType } = require('graphql')
const app = express()

const schema = new GraphQLSchema({
    query: new GraphQLObjectType({
        name: 'Helloworld',
        fields: () => ({
            message: {
                 type: GraphQLString,
                 resolve: () => 'Hello world'

            }
        })
    })
})
app.use('/graphql', expressGraphQl({ 
    schema: schema,
    graphiql: true
 }))
const port = 3000

app.listen(port, () => console.log(`Example app listening on port ${port}!`))